﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("your number = ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine(new string('-', 20));
            if (a % 2 == 0)
            {
                Console.WriteLine($"number {a} is even ");
            }
            else
            {
                Console.WriteLine($"number {a} is odd ");
            }
        }
    }
}
